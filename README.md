# SYKWEB evaluation - Gallery #

Test duration: 3 days.

### Before you begin ###
Please create a new branch with your name: eg. for John Smith create a branch called john-smith and do all your work on there.

### Objective ###

You must create a single web page that shows an image gallery.

The gallery will consist of a horizontally-scrollable list of thumbnails (small images).
When you click on a thumbnail a large version of the selected image will appear.

The web page must fetch the posts from this endpoint when it is loaded: http://jsonplaceholder.typicode.com/photos

Details for the gallery:

* Each thumbnail will have it's album id covering it on the bottom right corner. This reference/ID must be clearly visible but mustn't block the thumbnail (small image).
* The image title must appear in a tooltip when you hover over the thumbnail, use this library http://www.opentip.org/
* The image title must also appear as a caption for the enlarged image.
* The horizontal list of thumbnails must be _draggable_. Eg: If I hold the mouse down on an image and drag left, the entire list must drag/scroll left.
* When you hover over a thumbnail an "X" symbol must appear on the top right corner, when you click on this X the thumbnail will disappear from the list.
* The web page must be responsive (design that fits all screen sizes).

### Bonus ###

Bonus points for good Javascript animations.

Bonus points for an attractive design.

Bonus points for using React.js or Angular.js


### Tips ###

* `commit` often and with well-written messages.
* `commit` messages can be in French or English
* Use whatever libraries and tools you feel will help your each the objective.
* Web applications must _look great_ as well as _function well_.
* Quality over quantity, always.

### Help ###
For clarification or other queries contact saemie@chouchane.com

Bonne chance.